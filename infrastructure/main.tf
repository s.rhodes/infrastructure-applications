provider "google" {
  project = var.project.id
  region  = var.project.region
  zone    = var.project.zone
}

resource "google_project_service" "container_api" {
  service = "container.googleapis.com"

  disable_dependent_services = true
}

resource "google_project_service" "compute_api" {
  service = "compute.googleapis.com"

  disable_dependent_services = true
}

resource "google_compute_network" "vpc" {
  name                    = "${var.project.name}-vpc"
  auto_create_subnetworks = var.network.auto_create_subnetworks
  depends_on = [
    google_project_service.container_api,
    google_project_service.compute_api
  ]
}

resource "google_compute_subnetwork" "gke_subnet" {
  name          = "${var.project.name}-subnet"
  region        = var.project.region
  network       = google_compute_network.vpc.name
  ip_cidr_range = var.network.subnet_cidr
}

resource "google_container_cluster" "gke_cluster" {
  name               = "${var.project.name}-gke"
  location           = var.project.region
  network            = google_compute_network.vpc.name
  subnetwork         = google_compute_subnetwork.gke_subnet.name
  initial_node_count = var.node_config.initial_node_count
remove_default_node_pool = true
  deletion_protection      = false
  node_config {
    machine_type = var.node_config.sku
  }
}
