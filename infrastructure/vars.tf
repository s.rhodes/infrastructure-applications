
variable "project" {
  type = object({
    id     = string
    name   = string
    region = string
    zone   = string
  })
  default     = null
  description = "Project specific values"
}

variable "network" {
  type = object({
    subnet_cidr             = string
    auto_create_subnetworks = bool
  })
  default     = null
  description = "Network definitions."
}


variable "node_config" {
  type = object({
    sku                = string
    initial_node_count = number
  })
  default     = null
  description = "GKE Cluster Node configuration"
}
