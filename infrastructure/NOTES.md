# Stevens Notes

## First Time setup 

### Google Cloud SDK for MAC

Google Cloud SDK is required to cconnect to Google Cloud. Following are commands to configure MAC (or Linux) via Brew.

```bash
brew update
brew install --cask google-cloud-sdk
gcloud init
gcloud auth application-default login
```

## Stand up Infrastructure

```bash
terraform apply -var-file=default.tfvars
```

### Connect to GKE Cluster

First time using Google Cloud and wanting to connect to GKE Cluster you will need to install Kubectl AUTH plugin for gcloud.

```bash
gcloud components install gke-gcloud-auth-plugin
```

Pull the Credentials from the cluster to support use of `kubectl` and K8S related tools.

```bash
gcloud container clusters get-credentials gke-cluster --zone us-central1
```
