project = {
  id     = "rising-mercury-418600"
  name   = "srhodes-demo"
  region = "us-central1"
  zone   = "us-central1-a"
}
node_config = {
  sku                = "e2-medium"
  initial_node_count = 1
}

network = {
  subnet_cidr             = "172.16.30.0/24"
  auto_create_subnetworks = false
}