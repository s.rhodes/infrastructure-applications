# Infrastructure

## Summary

Terraform Code to stand up new GKE Cluster purely for demonstration purposes. This is NOT recommended for Production use. This code is creating a cluster using publicly accessible endpoints.  

Refer to NOTES.md if you are needing details on how to use this code base to create Google Cloud resources included in this code base.

<!-- BEGIN_TF_DOCS -->
## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_google"></a> [google](#provider\_google) | 5.22.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [google_compute_network.vpc](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_network) | resource |
| [google_compute_subnetwork.gke_subnet](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_subnetwork) | resource |
| [google_container_cluster.gke_cluster](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/container_cluster) | resource |
| [google_project_service.compute_api](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/project_service) | resource |
| [google_project_service.container_api](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/project_service) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_network"></a> [network](#input\_network) | Network definitions. | <pre>object({<br>    subnet_cidr             = string<br>    auto_create_subnetworks = bool<br>  })</pre> | `null` | no |
| <a name="input_node_config"></a> [node\_config](#input\_node\_config) | GKE Cluster Node configuration | <pre>object({<br>    sku                = string<br>    initial_node_count = number<br>  })</pre> | `null` | no |
| <a name="input_project"></a> [project](#input\_project) | Project specific values | <pre>object({<br>    id     = string<br>    name   = string<br>    region = string<br>    zone   = string<br>  })</pre> | `null` | no |

## Outputs

No outputs.
<!-- END_TF_DOCS -->